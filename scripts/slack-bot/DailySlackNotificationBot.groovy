import groovyx.net.http.ContentType
import groovyx.net.http.*
import static groovyx.net.http.Method.*
import groovy.json.JsonOutput
import groovyx.net.http.ContentType
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.RESTClient
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.search.SearchProvider
import com.atlassian.jira.issue.search.SearchQuery
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.jql.parser.DefaultJqlQueryParser
import java.util.Base64
import com.onresolve.scriptrunner.canned.jira.admin.GlobalConstant

/*
class ProductSupportEngineer {
    def empId
    def username
    def fullname
    def filterId

    ProductSupportEngineer(empId, username, fullname, filterId) {
        this.empId    = empId
        this.username = username
        this.fullname = fullname
        this.filterId = filterId
    }
}
*/

class SlackConfig {

    static SLACK_CHANNELS = 'kl-support'
    //static SLACK_CHANNELS = 'supportteam'
    //static SLACK_CHANNELS = 'razi-testing'
    static SLACK_API_POST_MESSAGE_URL = 'https://slack.com/api/chat.postMessage'

    static BOT_NAME = 'Handsome Support Bot'
    static BOT_ICON = ':bot_support_1:'

}

class Employee {

    static List<Integer> src_ppl_ids = [
            223, // Joana Choules
            225, // Jon Chin
            245, // Kristian Walker
            239, // Katy Kelly
            250, // Lee Wonnacot
            296, // Ram Kumar
            371, // Bobby Bailey
            378, // Joaquin Fernandez
            700, // Der Lun
            747, // Raziman Dom
            749, // Winnie Koh
            750, // Benz Kek
            751, // Kate Kabir
            758, // Joe Wai
            761, // Joe Baldwin
            762, // JJ Pang
            766, // Tiong Yao
            783, // Helmt Ibrahim
            794, // Max Lim
            812, // Amir Mustapha
            823, // Putri Kamarudin
            824, // Ifran Mazuki
            828, // Amirul Omar
            842, // KD Danial
    ]

    static Map<String, String> product_support_usernames = [
            "223":"jchoules", // Joana Choules
            "225":"jloong", // Jon Chin
            "245":"kwalker", // Kristian Walker
            "239":"kkelly", // Katy Kelly
            "250":"lwonnacott", // Lee Wonnacot
            "296":"rkumar", // Ram Kumar
            "371":"rbailey", // Bobby Bailey
            "378":"jfernandez", // Joaquin Fernandez
            "700":"dlooi", // Der Lun
            "747":"rdom", // Raziman Dom
            "749":"wkoh", // Winnie Koh
            "750":"bkek", // Benz Kek
            "751":"kkabir", // Kate Kabir
            "758":"jwai", // Joe Wai
            "761":"jbaldwin", // Joe Baldwin
            "762":"jpang", // JJ Pang
            "766":"tlihyao", // Tiong Yao
            "783":"hibrahim", // Helmt Ibrahim
            "794":"mlim", // Max Lim
            "812":"amustapha", // Amir Mustapha
            "823":"pkamarudin", // Putri Kamarudin
            "824":"imazuki", // Ifran Mazuki
            "828":"aomar", // Amirul Omar
            "842":"kdanial",  // KD Danial
    ]

    /*
    static List<ProductSupportEngineer> productSupportEngineerList = [
        new ProductSupportEngineer("0", "jchoules", "Joanna Choules", "19905"),
        new ProductSupportEngineer("0", "jloong", "John Chin", "18678"),
        new ProductSupportEngineer("0", "kwalker", "Kristian Walker", "19904"),
        new ProductSupportEngineer("0", "kkelly", "Katy Kelly", "19910"),
        new ProductSupportEngineer("0", "lwonnacott", "Lee Wonnacot", "19908"),
        new ProductSupportEngineer("0", "rkumar", "Ram Kumar", "18680"),
        new ProductSupportEngineer("0", "rbailey", "Bobby Bailey", "19906"),
        new ProductSupportEngineer("0", "jfernandez", "Joaquin Fernandez", "19907"),
        new ProductSupportEngineer("700", "dlooi", "Der Lun", "18692"),
        new ProductSupportEngineer("0", "rdom", "Raziman Dom", "18676"),
        new ProductSupportEngineer("0", "wkoh", "Winnie Koh", "18687"),
        new ProductSupportEngineer("0", "bkek", "Benz Kek", "18689"),
        new ProductSupportEngineer("0", "kkabir", "Kate Kabir", "18694"),
        new ProductSupportEngineer("0", "jwai", "Joe Wai", "19909"),
        new ProductSupportEngineer("0", "jbaldwin", "Joe Baldwin", "19906"),
        new ProductSupportEngineer("0", "jpang", "JJ Pang", "18690"),
        new ProductSupportEngineer("0", "tlihyao", "Tiong Yao", "18706"),
        new ProductSupportEngineer("0", "hibrahim", "Helmy Ibrahim", "18704"),
        new ProductSupportEngineer("0", "mlim", "Max Lim", "18708"),
        new ProductSupportEngineer("0", "amustapha", "Amir Mustapha", "19305"),
        new ProductSupportEngineer("0", "pkamarudin", "Putri Kamarudin", "18906"),
        new ProductSupportEngineer("0", "imazuki", "Irfan Mazuki", "18904"),
        new ProductSupportEngineer("0", "aomar", "Amirul Omar", "19305"),
        new ProductSupportEngineer("0", "kdanial", "KD Danial", "18702"),
    ]
    */

    static Map<String, String> product_support_filter_id = [
            "jchoules":"19905", // Joana Choules
            "jloong":"18678", // John Chin
            "kwalker":"19904", // Kristian Walker
            "kkelly":"19910", // Katy Kelly
            "lwonnacott":"19908", // Lee Wonnacot
            "rkumar":"18680", // Ram Kumar
            "rbailey":"19906", // Bobby Bailey
            "jfernandez":"19907", // Joaquin Fernandez
            "dlooi":"18692", // Der Lun
            "rdom":"18676", // Raziman Dom
            "wkoh":"18687", // Winnie Koh
            "bkek":"18689", // Benz Kek
            "kkabir":"18694", // Kate Kabir
            "jwai":"18688", // Joe Wai
            "jbaldwin":"19909", // Joe Baldwin
            "jpang":"18690", // JJ Pang
            "tlihyao":"18706", // Tiong Yao
            "hibrahim":"18704", // Helmt Ibrahim
            "mlim":"18708", // Max Lim
            "amustapha":"19305", // Amir Mustapha
            "pkamarudin":"18906", // Putri Kamarudin
            "imazuki":"18904", // Ifran Mazuki
            "aomar":"19305", // Amirul Omar
            "kdanial":"18702",  // KD Danial
    ]

    // Squad that is working from Sunday to Thursday
    static def squad_135 = [
            "bkek",
            "wkoh",
            "kkabir",
            "pkamarudin",
            "imazuki",
            "mlim"
    ] as List<String>

    // Squad that is working from Tuesday to Saturday
    static def squad_246 = [
            "jwai",
            "amustapha",
            "jpang",
            "tlihyao",
            "hibrahim",
            "aomar"
    ] as List<String>

}

// Slack message builder
def slackMessage = ":female-construction-worker: [*Testing Phase*] :male-construction-worker:\n\n"
slackMessage += "*Ticket updates:*"
slackMessage += "\n"
slackMessage += breachMessage()
slackMessage += "\n"
slackMessage += highPriorityMessage()
slackMessage += "\n\n"
slackMessage += holidayMessage()
slackMessage += "\n\n"
slackMessage += notWorkingMessage()

// Request body for slack post message api
def requestBody = [
        channel: SlackConfig.SLACK_CHANNELS,
        username: SlackConfig.BOT_NAME,
        icon_emoji: SlackConfig.BOT_ICON,
        text: slackMessage
] as Map

// Send the message to slack api
postRestAPI(requestBody, SlackConfig.SLACK_API_POST_MESSAGE_URL, decoder(GlobalConstant.SLACK_TOKEN))

// Decode encoded token
def decoder(encodedString) {
    def decodedBytes = Base64.getDecoder().decode(encodedString) as byte[]
    def decodedString = new String(decodedBytes)

}

/**
 * Generic post API
 * @param requestBody
 * @param hostUrl
 * @param token
 */

void postRestAPI(requestBody, hostUrl, token) {

    def http = new HTTPBuilder(hostUrl)

    http.setHeaders([Authorization: "Bearer ${token}"])
    http.request(POST) {

        body = requestBody
        requestContentType = ContentType.JSON

        response.success = { resp ->
            log.warn "POST Success - ${resp.status}"
        }

        response.failure = { resp ->
            log.warn "POST Failed - ${resp.status}"
        }
    }

}

/**
 * Generic get API
 * @param hostUrl
 * @param endpoint
 * @param queryParam
 * @return
 */

def getRestAPI(def hostUrl, def endpoint, queryParam) {

    def httpBuilder = new HTTPBuilder(hostUrl)

    httpBuilder.get(
            path: endpoint,
            //requestContentType: ContentType.JSON,
            contentType : 'application/json',
            //query: [start: todayStr, end: todayStr],
            query: queryParam,
            //query: [start: '2021-07-22', end: '2021-07-22'],
    ) {
        HttpResponseDecorator resp, reader -> reader
    }

}

/**
 * Generate message of team on leave
 * @return
 */

def holidayMessage() {

    // Date Format
    String dateFmt = 'yyyy-MM-dd'
    Date today = new Date()
    String todayStr = today.format(dateFmt)

    def externalUrl = "https://${decoder(GlobalConstant.BAMBOO_TOKEN)}:x@api.bamboohr.com/api/gateway.php/adaptavist/"
    def endPoint = "v1/time_off/whos_out"
    def queryParam = [start: todayStr, end: todayStr]

    //def hols_today = GlobalMethod.getRestAPI(externalUrl, endPoint, queryParam) as ArrayList
    def hols_today = getRestAPI(externalUrl, endPoint, queryParam) as ArrayList
    def src_hols_today = hols_today.findAll { Employee.src_ppl_ids.contains(it.employeeId) } as List<Map>

    def delim = '\n• ' as String
    def slack_list = { delim + it.join(delim) }

    def friendly = {
        "${it.name} (${it.end == today ? 'returns next working day' : 'until ' + it.end + ' inclusive'}) - " + userTicketMessage(Employee.product_support_usernames[it.employeeId.toString()])
    }

    def hols = src_hols_today.isEmpty()
            ? 'No holidays today.'
            : "*Holidays today*: ${slack_list(src_hols_today.collect { friendly(it) })}" as String

    return hols

}

/**
 * Generate message of squads' weekend
 * @return message
 */

def notWorkingMessage() {

    Date today = new Date()
    String dayOfWeek = today.format('EEEE')

    def message = '''*Scheduled Weekend:*''' as String

    if (dayOfWeek == 'Friday' || dayOfWeek == 'Saturday') {
        Employee.squad_135.each {
            message = message + "\n • ${ComponentAccessor.userManager.getUserByName(it).displayName}"
        }

    } else if (dayOfWeek == 'Sunday' || dayOfWeek == 'Monday') {
        Employee.squad_246.each {
            message = message + "\n • ${ComponentAccessor.userManager.getUserByName(it).displayName}"
        }

    } else {
        return ""
    }

    return message

}

/**
 * Method to get number of issues
 * @param searchQuery
 * @return number of issues
 */

def getTicketCounts(searchQuery) {

    def jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser) as JqlQueryParser
    def searchProvider = ComponentAccessor.getComponent(SearchProvider) as SearchProvider
    def issueManager = ComponentAccessor.issueManager
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser

    def query = jqlQueryParser.parseQuery(searchQuery)
    def searchResults = searchProvider.search(SearchQuery.create(query, user), PagerFilter.unlimitedFilter)

    searchResults.results.each { documentIssue ->
        def key = documentIssue.document.fields.find { it.name() == "key" }.stringValue()
        issueManager.getIssueObject(key)
    }

    return searchResults.results.size()
}

/**
 * Generate message and URL of breaching tickets
 * @return message
 */

def breachMessage() {

    // JQL Saved filter not working in scriptrunner job
    //def searchQuery = """filter = "All Support Projects - Breach in 6 hours" """

    def searchQuery = """ project in (SRJCLSUP, SRJSUP, SRBSUP, LICSUP, CONFORAH, PCSUP, SRCONFSUP, TRELLO, CFMSUP, FORMSUP, ATDSUP, CFSUP, RSUP, TBSUP, SRCLSUP, ABJSUP, PRODSUP, SRBOOSUP, ABSUP) 
    						AND (status = "With Support" OR status = "To Do") 
                            AND status != "SLA Paused"
                            AND ("With Support" < remaining(6h) OR Acknowledgement < remaining(6h)) 
                            ORDER BY "With Support" ASC, Acknowledgement ASC """

    def jiraBaseURL = com.atlassian.jira.component.ComponentAccessor.getApplicationProperties().getString("jira.baseurl")
    def urlFilter = "${jiraBaseURL}/issues/?filter=18669"

    def message = " • <" + urlFilter + "|" + getTicketCounts(searchQuery) + ">" + " ticket(s) will breach in less than 6 hours today"

    return message

}

/**
 * Generate message and URL of high priority tickets
 * @return message
 */

def highPriorityMessage() {

    // JQL Saved filter not working in scriptrunner job
    //def searchQuery = """filter = "All Support Projects - L1" """

    def searchQuery = """ project in (SRJCLSUP, SRJSUP, SRBSUP, LICSUP, CONFORAH, PCSUP, SRCONFSUP, TRELLO, CFMSUP, FORMSUP, ATDSUP, CFSUP, RSUP, TBSUP, SRCLSUP, ABJSUP, PRODSUP, SRBOOSUP, ABSUP) 
    						AND (status = "With Support" OR status = "To Do") 
                            AND priority = L1  
                            ORDER BY "With Support" ASC, Acknowledgement ASC """

    def jiraBaseURL = com.atlassian.jira.component.ComponentAccessor.getApplicationProperties().getString("jira.baseurl")
    def urlFilter = "${jiraBaseURL}/issues/?filter=19911"

    def message = " • <" + urlFilter + "|" + getTicketCounts(searchQuery) + ">" + " ticket(s) are at L1 priority"

    return message

}

/**
 * Get total tickets in user's backlog
 * @param username
 * @return Total tickets in user's backlog and URL message
 */

def userTicketMessage(username) {

    // JQL Saved filter not working in scriptrunner job
    //def searchQuery = """filter = "KL Support Agent Backlog" AND assignee in (${username}) ORDER BY "With Support" ASC, Acknowledgement ASC"""

    def searchQuery = """ assignee = ${username} AND status != "Waiting for Customer" AND status != "SLA Paused" AND resolution = EMPTY ORDER BY "With Support" ASC, Acknowledgement ASC """

    def totalTickets = getTicketCounts(searchQuery)
    def jiraBaseURL = com.atlassian.jira.component.ComponentAccessor.getApplicationProperties().getString("jira.baseurl")

    // User's filter id
    def filterId = Employee.product_support_filter_id.get(username)
    def urlFilter =  "${jiraBaseURL}/issues/?filter=${filterId}"

    // Create a message with a link
    def messageWithURL = "<" + urlFilter + "|" + totalTickets + ">"

    return messageWithURL + " ticket(s) in user's backlog"

}